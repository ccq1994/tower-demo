# Ansible Tower/AWX Clustering Demo

This repo contains the project files and notes for creating an AWX or Ansible Tower cluster.
This repo is authored by Chad Quinlan, originally for an A Cloud Guru Discord Discourse event.

## Startup

The user-data script included in this system immediately installs EPEL and ansible repos.
It also installs several tools.
You can uncomment the update line if you're interested in a longer-running server.
To view startup script status, run `tail -f /var/log/cloud-init-output.log`.

## Tower Download & Configure

Tower download and extract:

```shell
wget https://releases.ansible.com/ansible-tower/setup/ansible-tower-setup-latest.tar.gz
tar xvzf ansible-tower-setup-latest.tar.gz
```

Now we need to configure the inventory file: `vim ansible-tower-setup-*/inventory`.
Comment out or delete the `localhost` line under the `[tower]` group heading;
all nodes must be managed the same way in a cluster.
Add your **private** host IP addresses under `[tower]`.
Add a specific host to the `[database]` group heading;
this could be an external dedicated DB server or could be otherwise configured for an external DB.

Set a value for `admin_password`, `pg_host`, `pg_port`, and `pg_password`.
Note the default pg_port is `5432`.

> **Important note**: Do not use special characters in the `pg_password` value, due to Ansible documentation.

Add the following lines under the `[all:vars]` heading to configure SSH and user details:

```
ansible_ssh_common_args='-o StrictHostKeyChecking=no'
ansible_ssh_user=ec2-user
ansible_ssh_private_key_file=/home/ec2-user/.ssh/id_rsa
ansible_become=true
```

Your configuration should look similar to this, using your own IPs and passwords.

```
[tower]
# localhost ansible_connection=local
172.31.9.194
172.31.40.115
172.31.30.21

[automationhub]

[database]
172.31.30.21

[all:vars]
admin_password='redhat123'
ansible_ssh_common_args='-o StrictHostKeyChecking=no'
ansible_ssh_user=ec2-user
ansible_ssh_private_key_file=/home/ec2-user/.ssh/id_rsa
ansible_become=true

pg_host='172.31.30.21'
pg_port='5432'

pg_database='awx'
pg_username='awx'
pg_password='redhat123'
pg_sslmode='prefer'  # set to 'verify-full' for client-side enforced SSL
...
```

## Complete Setup

Continue with the installation process.

```shell
sudo ansible-tower-setup-*/setup.sh
```

Once the playbook has completed, you can log into any of the Tower hosts using the public IPs listed in the Terraform output.

You will need a RedHat developer account or a purchased license in order to proceed with the setup.
Log into the host using `admin` for the username and the `admin_password` value for the password.

> Once you are done with this demo, don't forget to `terraform destroy` your instances to prevent significant charges!