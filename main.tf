provider "aws" {
  region = "us-east-1"
  default_tags {
    tags = {
      Presentation = "A Cloud Guru - Discord Discourse"
      Topic        = "AWX/Ansible Tower Clustering"
      Application  = "Ansible Tower"
    }
  }
}

variable "public_ip_cidr_block" {
  description = "Public IP CIDR block to use for SG"
  type        = string
}

resource "tls_private_key" "main" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "private_key" {
  content         = tls_private_key.main.private_key_pem
  filename        = "ansible-tower.pem"
  file_permission = "0600"
}

resource "aws_key_pair" "main" {
  key_name   = "ansible-tower"
  public_key = tls_private_key.main.public_key_openssh
}

data "aws_ami" "rhel8" {
  most_recent = true
  owners      = ["309956199498"]

  filter {
    name   = "name"
    values = ["RHEL-8*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
}

data "aws_subnet_ids" "main" {
  vpc_id = "vpc-62b61104"
}

resource "aws_security_group" "main" {
  name        = "inbound_home"
  description = "Allow all inbound traffic from home"
  vpc_id      = "vpc-62b61104"

  ingress {
    description = "CQ Home"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.public_ip_cidr_block]
    self        = true
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_instance" "main" {
  count                       = 3
  instance_type               = "c5.xlarge"
  ami                         = data.aws_ami.rhel8.id
  key_name                    = aws_key_pair.main.key_name
  ebs_optimized               = true
  subnet_id                   = "subnet-1c3c5931"
  associate_public_ip_address = true
  root_block_device {
    volume_size = 50
    volume_type = "gp3"
  }
  vpc_security_group_ids = [aws_security_group.main.id]
  user_data              = file("user_data.sh")
  tags = {
    Name = "tower-cluster-${count.index + 1}"
  }
  provisioner "file" {
    source      = "ansible-tower.pem"
    destination = "/home/ec2-user/.ssh/id_rsa"

    connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = local_file.private_key.content
      host        = self.public_ip
    }
  }
  provisioner "remote-exec" {
    inline = [
      "chmod 0600 ~/.ssh/id_rsa"
    ]

    connection {
      type        = "ssh"
      user        = "ec2-user"
      private_key = local_file.private_key.content
      host        = self.public_ip
    }
  }
}

output "public_ip_addresses" {
  value = aws_instance.main.*.public_ip
}

output "private_ip_addresses" {
  value = aws_instance.main.*.private_ip
}